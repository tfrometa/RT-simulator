function varargout = RTSimulator(varargin)
%
% This program simulates  biological radiation effects (BRE) for calculating TCP using the minimum dose in tumor region 
% and using an estimated equivalent uniform dose(EUD)
% By means random generation of dose
% It can be sure there are a Minimum and Maximum aborbed energy (Emin and Emax respectively), 
% therefore the Emean will be between these two values of absorbed energies, then the Dose=Emean/dmass 
% will be between the respective dose to Emin 
% and Emax, Dmin and Dmax values. 
% We assumed the values the Dose have a Cumulative distribution fucntion ( CDF) type Tpdf
% which lets us to generate radom values od Dose for Tumor and NT
%
% % For each virtual smiluation (VS)is determined the condiction total K is aproximated to 100%, 
% When this condition is satisfied is increased the number of TCOK as 
% TCOK=TCOK+1, after the all VS then TCP=TCOK/nel. The condition for NT complication is 
% Keqmin <= Keq. Similar to TCP, then % NTCP=NTCOK/nel. 
% In this program we use the SMp K(d,n,SL,rt,(Tk,Tpot))and SMp SL(d,n,SL,rt,(Tk,Tpot))formalisms
%
%%%%% Now, different to its previous version, the K,SL,KSL and SLSL are radomly generated 
%%%%% in each fraction, even in the first. 

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RTSimulator_OpeningFcn, ...
                   'gui_OutputFcn',  @RTSimulator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function RTSimulator_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = RTSimulator_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%% Input for tumor %%%%%%%%%%%%%%%%%%%%%%%%

function dminKT_Callback(hObject, eventdata, handles)                      % Minimum dose of cell kill for Tumor
    global dminKT 
    dminKT=str2double(get(hObject,'String'));
    
function dminKT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dmaxKT_Callback(hObject, eventdata, handles)                      % Maximum dose of cell kill for Tumor
    global dmaxKT 
    dmaxKT=str2double(get(hObject,'String'));
    
function dmaxKT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pKT_Callback(hObject, eventdata, handles)                         % Power of the SMp cell kill for Tumor
    global pKT 
    pKT=str2double(get(hObject,'String'));
    
function pKT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dminSLT_Callback(hObject, eventdata, handles)                     % Minimum dose of SL for Tumor
    global dminSLT
    dminSLT=str2double(get(hObject,'String'));
    
function dminSLT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dmlSLT_Callback(hObject, eventdata, handles)                      % Dose for most likelihood SL for Tumor
    global dmlSLT 
    dmlSLT=str2double(get(hObject,'String'));
    
function dmlSLT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function MaxprobSLT_Callback(hObject, eventdata, handles)                  % Maximum probability for SL for Tumor 
    global MaxprobSLT dmlSLT dminKT dmaxKT pKT    
    MaxprobSLT=str2double(get(hObject,'String'))/100;                      % 1/100 for from percent to number
    
function MaxprobSLT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pSLT1_Callback(hObject, eventdata, handles)                       % Power 1 of the SMp SL(d) for Tumor
    global pSLT1 
    pSLT1=str2double(get(hObject,'String'));
    
function pSLT1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pSLT2_Callback(hObject, eventdata, handles)                       % Power 2 of the SMp SL(d) for Tumor
    global pSLT2 
    pSLT2=str2double(get(hObject,'String'));
    
function pSLT2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function PdKKeffT_Callback(hObject, eventdata, handles)                    % Percent of difference between K and Keff for Tumor
    global PdKKeffT 
    PdKKeffT=str2double(get(hObject,'String'))/100;
    
function PdKKeffT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function PdSLSLeffT_Callback(hObject, eventdata, handles)                  % Percent of difference between SL and SLeff for Tumor
    global PdSLSLeffT 
    PdSLSLeffT=str2double(get(hObject,'String'))/100;
    
function PdSLSLeffT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function RHTT_Callback(hObject, eventdata, handles)                        % Repair half time for Tumor
    global RHTT 
    RHTT=str2double(get(hObject,'String'))*(1/24);                         % (1/24) for converting from hour to day 
    
function RHTT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Tk_Callback(hObject, eventdata, handles)                          % Time begins tumor repopulation 
    global Tk 
    Tk=str2double(get(hObject,'String'));
    
function Tk_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Tpot_Callback(hObject, eventdata, handles)                        % Potential doubling time  
    global Tpot 
    Tpot=str2double(get(hObject,'String'));
    
function Tpot_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function CdenT_Callback(hObject, eventdata, handles)                       % Cell density for tumor 
    global CdenT 
    CdenT=str2double(get(hObject,'String'))*1e07;  
    
function CdenT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function VolT_Callback(hObject, eventdata, handles)                        % Volume tumor 
    global VolT 
    VolT=str2double(get(hObject,'String'))*1e-2; 
    
function VolT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%% Input for Normal tissue (NT) %%%%%%%%%%%%%%%

function dminKNT_Callback(hObject, eventdata, handles)                     % Minimum dose of cell kill for NT
    global dminKNT 
    dminKNT=str2double(get(hObject,'String'));
    
function dminKNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dmaxKNT_Callback(hObject, eventdata, handles)                     % Maximum dose of cell kill for NT
    global dmaxKNT 
    dmaxKNT=str2double(get(hObject,'String'));
    
function dmaxKNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pKNT_Callback(hObject, eventdata, handles)                        % Power of the SMp cell kill for NT
    global pKNT 
    pKNT=str2double(get(hObject,'String'));
    
function pKNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dminSLNT_Callback(hObject, eventdata, handles)                    % Minimum dose of SL for NT
    global dminSLNT 
    dminSLNT=str2double(get(hObject,'String'));
    
function dminSLNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function dmlSLNT_Callback(hObject, eventdata, handles)                     % Dose of most likelihood SL for NT
    global dmlSLNT 
    dmlSLNT=str2double(get(hObject,'String'));
    
function dmlSLNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function MaxprobSLNT_Callback(hObject, eventdata, handles)                 % Maximum probability for SL for NT 
    global MaxprobSLNT dmlSLNT dminKNT dmaxKNT pKNT    
    MaxprobSLNT=str2double(get(hObject,'String'))/100;                     % 1/100 for from percent to number
    
function MaxprobSLNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pSLNT1_Callback(hObject, eventdata, handles)                      % Power 1 of the SMp SL(d) for NT
    global pSLNT1 
    pSLNT1=str2double(get(hObject,'String'));
    
function pSLNT1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function pSLNT2_Callback(hObject, eventdata, handles)                      % Power 2 of the SMp SL(d) for NT  
    global pSLNT2 
    pSLNT2=str2double(get(hObject,'String'));
    
function pSLNT2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function PdKKeffNT_Callback(hObject, eventdata, handles)                   % Percent of difference between K and Keff for NT
    global PdKKeffNT 
    PdKKeffNT=str2double(get(hObject,'String'))/100;
    
function PdKKeffNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function PdSLSLeffNT_Callback(hObject, eventdata, handles)                 % Percent of difference between SL and SLeff for NT
    global PdSLSLeffNT 
    PdSLSLeffNT=str2double(get(hObject,'String'))/100;
    
function PdSLSLeffNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function RHTNT_Callback(hObject, eventdata, handles)                       % Repair half time for NT
    global RHTNT 
    RHTNT=str2double(get(hObject,'String'))*(1/24);                        % (1/24) for converting from hour to day 
    
function RHTNT_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%  Schedule  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dmin_Callback(hObject, eventdata, handles)                        % Minimum dose per fractions for Tumor 
    global dmin 
    dmin=str2double(get(hObject,'String')); 
    
function dmin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function eEUD_Callback(hObject, eventdata, handles)                        % Estimated EUD per fractions for NT
    global eEUD 
    eEUD=str2double(get(hObject,'String')); 
    
function eEUD_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function n_Callback(hObject, eventdata, handles)                           % Number of the fractions for n
    global n 
    n=str2double(get(hObject,'String')); 
    
function n_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%% Input for the simulations  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Percent1_Callback(hObject, eventdata, handles)                    % Percent for defining the left side of generated dose 
    global Percent1 
    Percent1=str2double(get(hObject,'String'))/100;                        % 1/100 for from percent to number
    
function Percent1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Percent2_Callback(hObject, eventdata, handles)                    % Percent for defining the right side of of generated dose
    global Percent2
    Percent2=str2double(get(hObject,'String'))/100;                        % 1/100 for from percent to number
    
function Percent2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function nve_Callback(hObject, eventdata, handles)                         % Number of virtual simulations   
    global nvs 
    nvs=str2double(get(hObject,'String'));
    
function nve_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Keqmin_Callback(hObject, eventdata, handles)                      % Minimum Keq for the End-point of interest 
    global Keqmin 
    Keqmin=str2double(get(hObject,'String'))/100;                          % 1/100 for from percent to number
    
function Keqmin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

 %%%%%%%%%%%%%%%% Functions %%%%%%%%%%%%%%%%%%%%

function Calculate_Callback(hObject, eventdata, handles)

    global dminKT dmaxKT  pKT dminSLT dmlSLT MaxprobSLT pSLT1 pSLT2
    global PdKKeffT PdSLSLeffT RHTT Tk Tpot CdenT VolT    
    global dminKNT dmaxKNT  pKNT dminSLNT dmlSLNT MaxprobSLNT pSLNT1 pSLNT2
    global PdKKeffNT PdSLSLeffNT RHTNT 
    global dmin eEUD n Percent1 Percent2 nvs Keqmin
    
    set(handles.TCP,'String','        ');     set(handles.NTCP,'String','       ');
    dmaxSLT=dmaxKT;                                                        % The dmaxSL is equal to dmaxK for Tumor and NT 
    dmaxSLNT=dmaxKNT; 
           
   
    
    STKNT=0; SFSLNT=0; STKT=0;                                             % For beginning the mean of Tumor and NT
    TCOK=0;                                                                % Tumor control OK
    NTCOK=0;                                                               % Normal tissue complication OK                                            
        
    TNCT=CdenT*VolT;                                                       % Total number of cells in Tumor 
    
    
    for i=1:nvs 
        gdmin=GenDose(dmin,Percent1,Percent2,n);                           % Generates the vector gdmin for Tumor 
        geEUD=GenDose(eEUD,Percent1,Percent2,n);                           % Generates the vector geEUD for NT 
         
                 %%%%%%%%%%%%% Calculation for NT %%%%%%%%%%%%
           
        tissue='N';
        DKKeffNT=1-PdKKeffNT;
        DSLSLeffNT=1-PdSLSLeffNT;
       
        for j=1:n
            K(j)=SMpK(geEUD(j),dminKNT,dmaxKNT,pKNT);
            SL(j)=SMpSL(geEUD(j),dminSLNT,dmlSLNT,MaxprobSLNT,dmaxSLNT,pSLNT1,pSLNT2);
            SLK(j)=SMpK(geEUD(j),dminKNT*DKKeffNT,dmaxKNT*DKKeffNT,pKNT); 
            SLSL(j)=SMpSL(geEUD(j),dminSLT*DSLSLeffNT,dmlSLT*DSLSLeffNT,MaxprobSLNT,dmaxSLNT*DSLSLeffNT,pSLNT1,pSLNT2);
        end
        
        [TKNT,FSLNT]=BRE(K,SL,n,RHTNT,SLK,SLSL,0,0,0,tissue);
        
       STKNT=STKNT+TKNT; SFSLNT=SFSLNT+FSLNT;                              % For mean calcualtions 
        
                   
        Keq=TKNT+(2*FSLNT)/3;                                              % Equivalent cell kill (Keq) 
       
        if Keqmin <= Keq                                                  
           NTCOK=NTCOK+1;
        end  
        
            %%%%%%%%%%%%% Calculation for Tumor %%%%%%%%%%%%%%
     
        tissue='T';
        DKKeffT=1-PdKKeffT;
        DSLSLeffT=1-PdSLSLeffT;
        for j=1:n
            K(j)=SMpK(gdmin(j),dminKT,dmaxKT,pKT);
            SL(j)=SMpSL(gdmin(j),dminSLT,dmlSLT,MaxprobSLT,dmaxSLT,pSLT1,pSLT2); 
            SLK(j)=SMpK(gdmin(j),dminKT*DKKeffT,dmaxKT*DKKeffT,pKT); 
            SLSL(j)=SMpSL(gdmin(j),dminSLT*DSLSLeffT,dmlSLT*DSLSLeffT,MaxprobSLT,dmaxSLT*DSLSLeffT,pSLT1,pSLT2);   
        end          
            
       [TKT,~]=BRE(K,SL,n,RHTT,SLK,SLSL,Tk,Tpot,TNCT,tissue); 
       
       STKT=STKT+TKT; 
       
       
        if TKT>=0.99;                                                      % Criteria for tumor control OK
           TCOK=TCOK+1;
        end
    end 
    
    MTKNT=STKNT/nvs; MFSLNT=SFSLNT/nvs; MTKT=STKT/nvs;                     % Mean calculations of K and SL for NT, toatl K for Tumor
    
    set(handles.TKT,'String',num2str(round(MTKT*100)));                                                            
    set(handles.TKNT,'String',num2str(round(MTKNT*100)));
    set(handles.FSLNT,'String',num2str(round(MFSLNT*100)));  

    
 
    TCP=TCOK/nvs; NTCP=NTCOK/nvs;                                          % Probabilistic definitions of the TCP and NTCP
    
    
    set(handles.TCP,'String',num2str(TCP*100));
    set(handles.NTCP,'String',num2str(NTCP*100));
    clear;

function [TK,FSL] = BRE(K,SL,n,RHT,SLK,SLSL,Tk,Tpot,TNCT,tissue)            % BRE fucntion  
    
    T=0;sumK=0;     
    for j=1:n
        if j==1                                                            % The first fraction 
           Ki(1)=K(1);
           SLi(1)=SL(1);
           TK=K(1); FSL=SLi(1);         
        else 
           IFT=IFTime(j);
           T=T+IFT;          
           Rt=CellRepair(SLi(j-1),IFT,RHT);                                % Cell repair
           if tissue=='T'
              Pt=CellRep(IFT,T,Tk,Tpot,TNCT);                              % Cell repopulation
           elseif tissue=='N'
              Pt=0; 
           end
           sumK=sumK+Ki(j-1);                                              % Sum of the cells before frction j
           Ki(j)=(1+2*Pt-sumK+(SLi(j-1)-Rt))*K(j)+(SLi(j-1)-Rt)*SLK(j);
           SLi(j)=(1+2*Pt-sumK+(SLi(j-1)-Rt))*SL(j)+(SLi(j-1)-Rt)*SLSL(j);
           
           if (tissue=='T')&& (Ki(j)>=1)
               TK=1;
               break
           end 
           if (tissue=='N')&& (Ki(j)>=1)
               TK=1;FSL=0;
               break
           end                
        end             
    end 
    
    
function rt=CellRepair(SL,IFT,RHT)                                         % Cell repair function 
    rt=SL*(1-exp(-(log(2)/RHT)*IFT));

function pt=CellRep(IFT,T,Tk,Tpot,TNCT)                                    % Cell repopulation function 
    pt=0; 
    if T >= Tk 
        pt=TNCT*exp(log(2)*IFT/Tpot);                                          % The repulation depends of the number of cells 
    end                                                                    % are survived in the fraction j
       
function SMpK=SMpK(d,dminK,dmaxK,pK)                                       % SMp K(d)function 
    if d <= dminK                                  
         SMpK=0;
    elseif ((d > dminK) && (d <= dmaxK))
         SMpK=((d-dminK)/(dmaxK-dminK))^pK;                   
    elseif (d > dmaxK)
         SMpK=1;       
    end  
      
  function SMpSL=SMpSL(d,dminSL,dmlSL,MaxprobSL,dmaxSL,pSL1,pSL2)          % SMp SL(d) fucntion
     if d <= dminSL                            
         SMpSL=0;
     elseif (d> dminSL) && (d<= dmlSL)
         SMpSL=(((d-dminSL)/(dmlSL-dminSL))^pSL1)*MaxprobSL;                 
     elseif (d> dmlSL) && (d<=dmaxSL)
         SMpSL=(((dmaxSL-d)/(dmaxSL-dmlSL))^pSL2)*MaxprobSL;                
     elseif (d>dmaxSL)
         SMpSL=0;       
     end   
     
function IFT=IFTime(j)                                                     % IFT function  
    IFT=1;                                                                 % IFT: Inter-fraction time         
    if j/5-int8(j/5)==0                                                    % For considering the two additional              %%%%%%%%%%%%%%%%%  NEW 
       IFT=IFT+2;                                                          % days in the weekend, Saturday and Sunday
    end        
                                                              
 function gDose=GenDose(dmean,Percent1,Percent2,n)                         % Generates nvs random values of the Dose  
                                                                           % between gDmin and gDmax
  dmin=dmean*(1-(Percent1));                                               % -Percent1 of Dmean
  dmax=dmean*(1+(Percent2));                                               % +Percent2 of Dmean                                           
                                                                           

    for j=1:n     
        r=rand(1);   
        if (r>=0) && (r<=(dmean-dmin)*(dmean-dmin)/((dmax-dmin)*(dmax-dmean)))
           gDose(j)=dmin+sqrt(r*(dmax-dmin)*(dmax-dmean));
        else
           gDose(j)=dmax-sqrt((1-r)*(dmax-dmin)*(dmax-dmean));
        end 
    end 
 
